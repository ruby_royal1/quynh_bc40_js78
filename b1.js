
// Lấy dữ liệu
function getEl(a) {
    return document.getElementById(a);

}

// Lếy số
var numArr = [];
function getNum() {
    // 
    var num = getEl("num-in").value * 1;
    numArr.push(num);
    getEl("txt-Arr").innerHTML = numArr;
    console.log("🚀 ~ file: b1.js:32 ~ getNum ~ numArr", numArr)
}



// KIỂM TRA SỐ NGUYÊNG TỐ
function checkPrime(a) {
    if (a < 2) {
        return false;
    } else if (a == 2) {
        return true;
    }
    else if (a % 2 == 0) {
        return false;
    }
    else {
        for (let i = 3; i <= Math.sqrt(a); i += 2) {
            if (a % i == 0) {
                return false;
            }
        }
    } return true;
}
//bài 1: Tổng số dương
function sumPos() {
    var sum = 0;
    for (var i = 0; i < numArr.length; i++) {
        (numArr[i] > 0 ? sum += numArr[i] : sum);

    }
    getEl("txt-2-result").innerHTML = sum;
}

//bài 2: Đếm số dương
function countPos() {
    var count2 = 0;
    for (var i = 0; i < numArr.length; i++) {
        (numArr[i] > 0 ? count2++ : count2);

    }
    getEl("txt-3-result").innerHTML = count2;
}

// BÀI 3: Số nhỏ nhất trong mảng
function minNum() {
    var min = numArr[0];
    for (var i = 0; i < numArr.length; i++) {
        (min > numArr[i] ? min = numArr[i] : min);

    }
    getEl("txt-4-result").innerHTML = min;
}

// Bài 4: Tìm số dương nhỏ nhất trong mảng
function minPos() {
    var min = 0;
    var pos = [];
    for (var index = 0; index < numArr.length; index++) {
        var temp = numArr[index];
        if (temp > 0) {
            pos.push(temp);
        }

    }
    if (pos.length > 0) {
        min = pos[0]
        for (let i = 0; i < pos.length; i++) {
            (min > pos[i] ? min = pos[i] : min);

        }
        getEl("txt-5-result").innerHTML = min;
    } else {
        getEl("txt-5-result").innerHTML = `không có số dương trong mảng!`;
    }


}

// Bài 5: Tìm số chẵn cuối cùng trong mảng. Nếu mảng không có giá trị chẵn thì trả về -1.

function lastEven() {
    var last = 0;
    var even = [];
    for (var index = 0; index < numArr.length; index++) {
        var temp = numArr[index];
        if (temp % 2 == 0) {
            even.push(temp);
        }

    }
    if (even.length > 0) {
        last = even[(even.length - 1)];
        getEl("txt-6-result").innerHTML = last;
    } else {
        getEl("txt-6-result").innerHTML = -1;
    }
}
// BÀI 6: ĐỔI CHỖ
function swap() {
    var num1 = getEl("num-1").value * 1;
    var num2 = getEl("num-2").value * 1;
    var temp = 0;
    temp = numArr[num1];
    numArr[num1] = numArr[num2];
    numArr[num2] = temp;
    getEl("txt-7-result").innerHTML = numArr;
}
// BÀI 7: Sắp xếp mảng theo thứ tự tăng dần 

function sortArr() {
    numArr = numArr.sort((a, b) => a - b);
    getEl("txt-8-result").innerHTML = numArr;
}

// BÀI 8: Tìm số nguyên tố đầu tiên trong mảng;

function firstPrime() {
    var pos = [];
    for (var index = 0; index < numArr.length; index++) {
        var temp = numArr[index];
        if (temp > 0) {
            pos.push(temp);

        }

    }
    console.log("🚀 ~ file: b1.js:138 ~ firstPrime ~  pos", pos)
    if (pos.length > 0) {
        for (var i = 0; i < pos.length; i++) {
            a = pos[i];
            if (checkPrime(a) == true) {
                getEl("txt-9-result").innerHTML = a;
                break;
            } else {
                getEl("txt-9-result").innerHTML = `KHông có số nguyên tố nào hết trơn!`
            }

        }
    } else {
        getEl("txt-9-result").innerHTML = `KHông có số nguyên tố nào hết trơn hết trọi!`
    }
}


// Bài 9:  Nhập thêm 1 mảng số thực, tìm xem trong mảng có bao nhiêu số nguyên?
// Lấy số bài 9

var numArr9 = [];
function getNum9() {
    // 
    var num9 = getEl("num-9-in").value * 1;
    numArr9.push(num9);
    getEl("txt-9-Arr").innerHTML = numArr9;

}


function countInt() {
    var cnt = 0;
    for (var x = 0; x < numArr9.length; x++) {
               
                if( Number.isInteger(numArr9[x])){
                    cnt++;
                } else cnt;
                // Number.isInteger(numArr9[x]) && n++

    }
    getEl("txt-10-result").innerHTML = cnt;
    console.log("🚀 ~ file: b1.js:179 ~ countInt ~ cnt", cnt)
}


// Bài 10: So sánh số lượng số dương và số lượng số âm xem số nào nhiều hơn.

function comparePos() {
    var pos=0;
    var neg=0;
    for (var x = 0; x < numArr.length; x++) {
               
        numArr[x]>=0?pos++:neg++;
        
}
if (pos>neg){
    getEl("txt-11-result").innerHTML = `Số phần tử dương > Số phần tử âm`; 
} else if(pos<neg){
    getEl("txt-11-result").innerHTML = `Số phần tử dương < Số phần tử âm`; 
} else{
    getEl("txt-11-result").innerHTML = `Số phần tử dương = Số phần tử âm`; 
}
}

